package es.yurimontero.demo.vo

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Todo(
    @PrimaryKey val id: Int = 0,
    val title: String = "",
    val completed: Boolean = false
) {

    fun update(title: String, completed: Boolean): Todo {
        return Todo(id, title, completed)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<Todo>() {

            override fun areItemsTheSame(oldItem: Todo, newItem: Todo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Todo, newItem: Todo): Boolean {
                return oldItem.title == newItem.title && oldItem.completed == newItem.completed
            }
        }
    }
}
