package es.yurimontero.demo.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import es.yurimontero.demo.ui.fragments.home.HomeFragment
import es.yurimontero.demo.ui.fragments.login.LoginFragment

@Suppress("unused")
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment
}
