package es.yurimontero.demo.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import es.yurimontero.demo.App
import es.yurimontero.demo.utils.AppExecutors
import javax.inject.Singleton

@Module(
        includes = [
            ActivityModule::class,
            DbModule::class,
            FragmentModule::class,
            NetworkModule::class,
            ViewModelModule::class]
)
class AppModule {

    @Singleton
    @Provides
    fun provideAppExecutors(): AppExecutors {
        return AppExecutors()
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(app: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Singleton
    @Provides
    fun provideApp(app: Application): App {
        return app as App
    }
}
