package es.yurimontero.demo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import es.yurimontero.demo.ui.fragments.home.HomeViewModel
import es.yurimontero.demo.ui.fragments.login.LoginViewModel
import es.yurimontero.demo.viewmodel.ViewModelFactory

@Module
@Suppress("unused")
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindMainViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(homeViewModel: LoginViewModel): ViewModel
}
