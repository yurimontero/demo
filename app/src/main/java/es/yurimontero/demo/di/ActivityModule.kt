package es.yurimontero.demo.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import es.yurimontero.demo.ui.MainActivity

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
