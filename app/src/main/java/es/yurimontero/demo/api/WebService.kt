package es.yurimontero.demo.api

import es.yurimontero.demo.vo.Todo
import retrofit2.Response
import retrofit2.http.GET

interface WebService {

    @GET("todos")
    suspend fun getTodo(): Response<List<Todo>>
}
