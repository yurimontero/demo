package es.yurimontero.demo.repository

import es.yurimontero.demo.api.WebService
import es.yurimontero.demo.db.dao.MainDao
import es.yurimontero.demo.utils.apiResponse
import es.yurimontero.demo.vo.Todo
import kotlinx.coroutines.delay
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val webService: WebService,
    private val mainDao: MainDao
) {

    companion object {
        private const val DUMMY_TIMEOUT = 1_200L
    }

    suspend fun getTodo() = apiResponse { webService.getTodo() }

    suspend fun addTodos(list: List<Todo>) = mainDao.clearAndInsert(list)

    fun getTodos(filter: String) = mainDao.get(filter)

    suspend fun update(item: Todo) = mainDao.update(item)

    suspend fun del(item: Todo) = mainDao.del(item)

    suspend fun checkCredentials(user: String, pass: String): Boolean {
        delay(DUMMY_TIMEOUT)
        return user == "yuri" && pass == "1234"
    }
}
