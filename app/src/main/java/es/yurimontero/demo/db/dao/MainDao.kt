package es.yurimontero.demo.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.Delete
import androidx.room.Transaction

import es.yurimontero.demo.vo.Todo

@Dao
interface MainDao {

    @Insert
    suspend fun add(list: List<Todo>)

    @Query("SELECT * FROM Todo WHERE completed LIKE '%' || :filter || '%'")
    fun get(filter: String): LiveData<List<Todo>>

    @Query("DELETE FROM Todo")
    suspend fun del()

    @Update
    suspend fun update(item: Todo)

    @Delete
    suspend fun del(item: Todo)

    @Transaction
    suspend fun clearAndInsert(list: List<Todo>) {
        del()
        add(list)
    }
}
