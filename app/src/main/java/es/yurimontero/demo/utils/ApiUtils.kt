package es.yurimontero.demo.utils

import es.yurimontero.demo.api.ApiResponse
import retrofit2.Response

@Suppress("Detekt.TooGenericExceptionCaught")
suspend fun <T : Any> apiResponse(call: suspend () -> Response<T>): ApiResponse<T> {
    return try {
        ApiResponse(call.invoke())
    } catch (e: Throwable) {
        ApiResponse(e)
    }
}
