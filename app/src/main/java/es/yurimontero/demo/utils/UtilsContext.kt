package es.yurimontero.demo.utils

import android.content.Context
import android.content.DialogInterface
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import androidx.annotation.RawRes
import androidx.appcompat.app.AlertDialog
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import es.yurimontero.demo.BuildConfig

inline fun <reified T> Context?.systemService() = this?.getSystemService(T::class.java)

fun Context?.inflate(@LayoutRes layout: Int): View {
    return LayoutInflater.from(this).inflate(layout, LinearLayout(this), false)
}

fun Context?.dialog(
    title: String?,
    message: String,
    okListener: ((DialogInterface) -> Unit)? = null,
    cancelListener: ((DialogInterface) -> Unit)? = null
) {
    this?.also {
        AlertDialog.Builder(it).apply {
            if (title.isNullOrEmpty().not()) {
                setTitle(title)
            }
            setMessage(message)

            setCancelable(false)
            if (okListener != null) {
                setPositiveButton(android.R.string.ok) { d, _ -> okListener.invoke(d) }
            } else {
                setPositiveButton(android.R.string.ok) { d, _ -> d.dismiss() }
            }
            if (cancelListener != null) {
                setNegativeButton(android.R.string.cancel) { d, _ -> cancelListener.invoke(d) }
            }
            create()
            show()
        }
    }
}

fun Context?.isNetworkAvailable(): Boolean {
    return (this?.getSystemService(Context.CONNECTIVITY_SERVICE) as?
            ConnectivityManager)?.activeNetworkInfo?.isConnected == true
}

fun Fragment.dialog(
    title: String?,
    message: String,
    okListener: ((DialogInterface) -> Unit)? = null,
    cancelListener: ((DialogInterface) -> Unit)? = null
) {
    this.context.dialog(title, message, okListener, cancelListener)
}

fun Fragment.isNetworkAvailable(): Boolean {
    return context.isNetworkAvailable()
}

fun Context?.vibrate(millis: Long) {
    systemService<Vibrator>()?.also { v ->
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            @Suppress("DEPRECATION")
            v.vibrate(millis)
        }
    }
}

fun Fragment.vibrate(millis: Long) {
    activity.vibrate(millis)
}

fun Context?.playSound(@RawRes ids: Int) {

//    val scanSound = MediaPlayer.create(this, ids)
//    scanSound?.setOnCompletionListener {
//        it.stop()
//        it.release()
//    }
//
//    scanSound?.start()
    val uri = ("android.resource://" + BuildConfig.APPLICATION_ID + "/" + ids).toUri()
    // val uri = (MediaStore.Audio.Media.INTERNAL_CONTENT_URI.toString() + "/" + 34).toUri()
    val r = RingtoneManager.getRingtone(this, uri)
    r.play()
}

fun Fragment.playSound(@RawRes ids: Int) {
    activity.playSound(ids)
}
