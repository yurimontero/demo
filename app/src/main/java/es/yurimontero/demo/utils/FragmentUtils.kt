package es.yurimontero.demo.utils

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import es.yurimontero.demo.R
import es.yurimontero.demo.utils.ANIM.FADE_IN_OUT
import es.yurimontero.demo.utils.ANIM.NONE
import es.yurimontero.demo.utils.ANIM.POP
import es.yurimontero.demo.utils.ANIM.SLIDE_LEFT_RIGHT
import es.yurimontero.demo.utils.ANIM.SLIDE_LEFT_RIGHT_WITHOUT_EXIT

fun AppCompatActivity?.replaceFragment(
    @IdRes id: Int,
    fragment: Fragment,
    addToBackStack: Boolean = false,
    animation: ANIM? = NONE
) {

    this?.supportFragmentManager?.beginTransaction()?.apply {
        when (animation) {
            POP -> setCustomAnimations(
                R.anim.anim_enter,
                R.anim.anim_exit,
                R.anim.anim_pop_enter,
                R.anim.anim_pop_exit
            )

            FADE_IN_OUT -> setCustomAnimations(
                R.anim.anim_frag_fade_in,
                R.anim.anim_frag_fade_out
            )

            SLIDE_LEFT_RIGHT -> setCustomAnimations(
                R.anim.slide_in_from_rigth,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_right
            )
            SLIDE_LEFT_RIGHT_WITHOUT_EXIT -> setCustomAnimations(R.anim.slide_in_from_rigth, 0)

            else -> setCustomAnimations(0, 0)
        }

        if (addToBackStack) {
            addToBackStack(fragment.javaClass.simpleName)
        }

        replace(id, fragment, fragment.javaClass.simpleName)
        commit()
    }
}

fun AppCompatActivity?.addFragment(
    @IdRes id: Int,
    fragment: Fragment,
    addToBackStack: Boolean = true,
    animation: ANIM? = SLIDE_LEFT_RIGHT
) {

    this?.supportFragmentManager?.beginTransaction()?.apply {
        when (animation) {
            POP -> setCustomAnimations(
                R.anim.anim_enter,
                R.anim.anim_exit,
                R.anim.anim_pop_enter,
                R.anim.anim_pop_exit
            )

            FADE_IN_OUT -> setCustomAnimations(
                R.anim.anim_frag_fade_in,
                R.anim.anim_frag_fade_out
            )

            SLIDE_LEFT_RIGHT -> setCustomAnimations(
                R.anim.slide_in_from_rigth,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_right
            )
            SLIDE_LEFT_RIGHT_WITHOUT_EXIT -> setCustomAnimations(R.anim.slide_in_from_rigth, 0)

            else -> setCustomAnimations(0, 0)
        }

        if (addToBackStack) {
            addToBackStack(fragment.javaClass.simpleName)
        }

        add(id, fragment, fragment.javaClass.simpleName)
        commit()
    }
}

fun AppCompatActivity?.cleanBackStack() {
    this?.supportFragmentManager?.also {
        for (i in 0 until it.backStackEntryCount) {
            it.popBackStack()
        }
    }
}
