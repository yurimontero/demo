package es.yurimontero.demo.utils

enum class ANIM {
    FADE_IN_OUT,
    NONE,
    POP,
    SLIDE_LEFT_RIGHT,
    SLIDE_LEFT_RIGHT_WITHOUT_EXIT
}
