package es.yurimontero.demo.utils.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckedTextView
import android.widget.SpinnerAdapter
import android.widget.TextView

@Suppress("UNCHECKED_CAST")
class SAdapter<T>(
    cnt: Context,
    private val viewBinder: (ViewHolder<T>).() -> Unit,
    private val dropBinder: (DropHolder<T>).() -> Unit
) : ArrayAdapter<T>(cnt, android.R.layout.simple_spinner_item), SpinnerAdapter {

    private val resource: Int = android.R.layout.simple_spinner_item
    private val dropDown: Int = android.R.layout.simple_spinner_dropdown_item

    var items: List<T> = emptyList()
        set(items) {
            field = items
            clear()
            addAll(field)
            notifyDataSetChanged()
        }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var view = convertView
        val viewHolder: ViewHolder<T>

        if (view == null) {
            view = LayoutInflater.from(parent.context).inflate(resource, parent, false)
            viewHolder = ViewHolder(view!!)
            viewHolder.text1.setSingleLine(false)
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as ViewHolder<T>
        }

        viewHolder.item = items[position]
        viewBinder(viewHolder)

        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {

        var view = convertView
        val dropHolder: DropHolder<T>

        if (view == null) {
            view = LayoutInflater.from(parent.context).inflate(dropDown, parent, false)
            dropHolder = DropHolder(view!!)
            view.tag = dropHolder
        } else {
            dropHolder = view.tag as DropHolder<T>
        }

        dropHolder.item = items[position]
        dropBinder(dropHolder)

        return view
    }

    class ViewHolder<T>(v: View) {
        var item: T? = null
        var text1: TextView = v.findViewById(android.R.id.text1)
    }

    class DropHolder<T>(v: View) {
        var item: T? = null
        var text1: CheckedTextView = v.findViewById(android.R.id.text1)
    }
}
