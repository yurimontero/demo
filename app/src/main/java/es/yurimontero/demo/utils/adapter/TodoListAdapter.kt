package es.yurimontero.demo.utils.adapter

import es.yurimontero.demo.R
import es.yurimontero.demo.utils.AppExecutors
import es.yurimontero.demo.vo.Todo
import kotlinx.android.synthetic.main.item_todo.*
import javax.inject.Inject

class TodoListAdapter @Inject constructor(appExecutors: AppExecutors) :
    BaseListAdapter<Todo>(appExecutors, R.layout.item_todo, Todo.DIFF_UTIL) {

    override fun bind(holder: ViewHolder, item: Todo, onClickListener: ((Todo) -> Unit)?) {
        holder.apply {

            tvId.text = "${item.id}"
            tvTitle.text = item.title
            cbCompleted.isChecked = item.completed

            vTouch.setOnClickListener {
                onClickListener?.invoke(item)
            }
        }
    }
}
