package es.yurimontero.demo.utils.adapter

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import es.yurimontero.demo.utils.AppExecutors
import es.yurimontero.demo.utils.inflate
import kotlinx.android.extensions.LayoutContainer

abstract class BaseListAdapter<T>(
    appExecutor: AppExecutors,
    @LayoutRes
    private val layoutRes: Int,
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, BaseListAdapter.ViewHolder>(
    AsyncDifferConfig.Builder<T>(diffCallback)
        .setBackgroundThreadExecutor(appExecutor.diskIO())
        .build()
) {

    var onClickListener: ((T) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.context.inflate(layoutRes))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bind(holder, getItem(position), onClickListener)
    }

    protected abstract fun bind(holder: ViewHolder, item: T, onClickListener: ((T) -> Unit)?)

    public override fun getItem(position: Int): T {
        return super.getItem(position)
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer
}
