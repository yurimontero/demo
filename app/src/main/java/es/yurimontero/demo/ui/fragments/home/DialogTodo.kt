package es.yurimontero.demo.ui.fragments.home

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.switchmaterial.SwitchMaterial
import es.yurimontero.demo.R
import es.yurimontero.demo.vo.Todo

class DialogTodo : DialogFragment() {

    companion object {
        const val TAG = "DialogTodo"
        private lateinit var item: Todo
        private lateinit var okListener: (Todo) -> Unit

        fun newInstance(item: Todo, okListener: (Todo) -> Unit): DialogTodo {
            DialogTodo.item = item
            DialogTodo.okListener = okListener
            return DialogTodo()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val v = View.inflate(context, R.layout.dialog_todo, null)
        val etTitle = v.findViewById<EditText>(R.id.etTitle)
        val swCompleted = v.findViewById<SwitchMaterial>(R.id.swCompleted)

        etTitle.setText(item.title)
        swCompleted.isChecked = item.completed

        return MaterialAlertDialogBuilder(context)
            .setView(v)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                okListener.invoke(item.update(etTitle.text.toString(), swCompleted.isChecked))
            }
            .create()
    }
}
