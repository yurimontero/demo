package es.yurimontero.demo.ui.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.yurimontero.demo.repository.MainRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {

    private val _loginStatus = MutableLiveData(LoginStatus())
    val loginStatus: LiveData<LoginStatus> = _loginStatus

    var user: String = ""
    var pass: String = ""

    fun clickLogin() {
        when {
            user.isBlank() -> _loginStatus.value = LoginStatus(userEmpty = true)
            pass.isBlank() -> _loginStatus.value = LoginStatus(passEmpty = true)
            else -> checkCredentials(user, pass)
        }
    }

    private fun checkCredentials(user: String, pass: String) {
        viewModelScope.launch {
            val hasCredentials = repository.checkCredentials(user, pass)
            if (hasCredentials) {
                _loginStatus.value = LoginStatus(loginOk = true)
            } else {
                _loginStatus.value = LoginStatus(loginKo = true)
            }
        }
    }
}
