package es.yurimontero.demo.ui.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import es.yurimontero.demo.R
import es.yurimontero.demo.di.Injectable
import es.yurimontero.demo.utils.adapter.TodoListAdapter
import es.yurimontero.demo.utils.dialog
import es.yurimontero.demo.vo.Todo
import kotlinx.android.synthetic.main.fragment_home.*
import timber.log.Timber
import javax.inject.Inject

class HomeFragment : Fragment(), Injectable {

    companion object {
        fun newInstance() = HomeFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: HomeViewModel by activityViewModels {
        viewModelFactory
    }

    @Inject
    lateinit var listAdapter: TodoListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initList()

        viewModel.homeStatus.observe(viewLifecycleOwner, Observer { homeStatus ->
            homeStatus?.apply {
                progress?.let(::showProgress)
                error?.let(::showError)
                dialog?.let(::showDialog)
            }
        })

        viewModel.list.observe(viewLifecycleOwner, Observer {
            listAdapter.submitList(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.actionAll -> {
                viewModel.filterAll()
                true
            }
            R.id.actionUncompleted -> {
                viewModel.filterUncompleted()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initList() {
        list?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            val divider = DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
            addItemDecoration(divider)

            listAdapter.onClickListener = {
                Timber.d("$it")
                viewModel.itemClicked(it)
            }

            adapter = listAdapter
        }
    }

    private fun showProgress(visibility: Int) {
        progressBar.visibility = visibility
    }

    private fun showError(error: String) {
        dialog("Error", error)
    }

    private fun showDialog(item: Todo) {

        childFragmentManager.beginTransaction().apply {
            childFragmentManager.findFragmentByTag(DialogTodo.TAG)?.let {
                remove(it)
            }
            addToBackStack(null)

            DialogTodo.newInstance(item, okListener = { item ->
                viewModel.save(item)
            }).show(this, DialogTodo.TAG)
        }
    }
}
