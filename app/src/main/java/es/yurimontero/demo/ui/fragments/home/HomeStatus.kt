package es.yurimontero.demo.ui.fragments.home

import es.yurimontero.demo.vo.Todo

class HomeStatus(
    val progress: Int? = null,
    val error: String? = null,
    val dialog: Todo? = null
)
