package es.yurimontero.demo.ui.fragments.login

data class LoginStatus(
    val loginOk: Boolean? = null,
    val loginKo: Boolean? = null,
    val userEmpty: Boolean? = null,
    val passEmpty: Boolean? = null
)
