package es.yurimontero.demo.ui.fragments.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.yurimontero.demo.api.ApiErrorResponse
import es.yurimontero.demo.api.ApiSuccessResponse
import es.yurimontero.demo.repository.MainRepository
import es.yurimontero.demo.vo.Todo
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {

    private val _filter = MutableLiveData<String>("")
    private val _list = Transformations.switchMap(_filter) { repository.getTodos(it) }
    private val _homeStatus = MutableLiveData<HomeStatus>()

    val list: LiveData<List<Todo>> = _list
    val homeStatus: LiveData<HomeStatus> = _homeStatus

    init {
        viewModelScope.launch {
            setProgressStatus(true)

            callRepository()

            setProgressStatus(false)
        }
    }

    fun itemClicked(item: Todo) {
        viewModelScope.launch {
            // repository.update(item.invert())
            _homeStatus.value = HomeStatus(dialog = item)
        }
    }

    fun save(item: Todo) {
        viewModelScope.launch {
            repository.update(item)
        }
    }

    fun filterAll() {
        _filter.value = ""
    }

    fun filterUncompleted() {
        _filter.value = "0"
    }

    private suspend fun callRepository() {
        when (val res = repository.getTodo()) {
            is ApiSuccessResponse -> {
                addParameters(res.body)
            }

            is ApiErrorResponse -> {
                setErrorStatus(res.errorMessage)
            }
        }
    }

    private fun setProgressStatus(isProgressIndicatorShown: Boolean) {
        _homeStatus.value =
            HomeStatus(progress = if (isProgressIndicatorShown) View.VISIBLE else View.GONE)
    }

    private fun setErrorStatus(errorMessage: String) {
        _homeStatus.value = HomeStatus(error = errorMessage)
    }

    private suspend fun addParameters(list: List<Todo>) {
        repository.addTodos(list)
    }
}
