package es.yurimontero.demo.ui.fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import es.yurimontero.demo.R
import es.yurimontero.demo.di.Injectable
import es.yurimontero.demo.ui.MainActivity
import es.yurimontero.demo.ui.fragments.home.HomeFragment
import es.yurimontero.demo.utils.replaceFragment
import kotlinx.android.synthetic.main.login_fragment.*
import javax.inject.Inject

class LoginFragment : Fragment(), Injectable {

    companion object {
        fun newInstance() = LoginFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: LoginViewModel by activityViewModels {
        viewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.loginStatus.observe(viewLifecycleOwner, Observer { loginStatus ->
            loginStatus?.apply {
                loginOk?.let {
                    (activity as? MainActivity)?.replaceFragment(
                        R.id.container,
                        HomeFragment.newInstance()
                    )
                }
                loginKo?.let {
                    MaterialAlertDialogBuilder(context)
                        .setTitle("Error")
                        .setMessage("Credentials do not belong to any user")
                        .setPositiveButton("Ok", null)
                        .show()
                }
                userEmpty?.let {
                    tilUser.error = "Field can't be empty"
                    tilUser.requestFocus()
                }
                passEmpty?.let {
                    tilPass.error = "Field can't be empty"
                    tilPass.requestFocus()
                }
            }
        })

        etUser.addTextChangedListener {
            viewModel.user = it.toString()
            tilUser.error = null
        }

        etPass.addTextChangedListener {
            viewModel.pass = it.toString()
            tilPass.error = null
        }

        btnLogin.setOnClickListener {
            viewModel.clickLogin()
        }
    }
}
